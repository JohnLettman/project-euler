#include <stdio.h>

const char* HEADER =
	"Project Euler - Problem #2\n"
	"John P. Lettman (johnlettman.com)\n"
	"<johnl@septmedia.com>\n\n";

unsigned int answer = 0;

void main()
{
	puts(HEADER);
	puts("Calculating the sum of all even numbered Fibonacci numbers under 4,000,000...");

	unsigned int p = 0, c = 1, temp;
	while(c < 4000000)
	{
		if(c % 2 == 0)
		{
			answer += c;
		}

		temp = c;
		c += p;
		p = temp;
	}

	printf("DONE!\nThe answer is %u.", answer);
}