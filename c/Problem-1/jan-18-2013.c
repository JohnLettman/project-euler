#include <stdio.h>
#define DISPLAY_MULTIPLES false

const char* HEADER =
	"Project Euler - Problem #1\n"
	"John P. Lettman (johnlettman.com)\n"
	"<johnl@septmedia.com>\n\n";

unsigned int answer = 0;

void main()
{
	puts(HEADER);
	puts("Calculating all multiples of 3 or 5 under 1000... ");

	unsigned short i = 1;
	unsigned short multiples = 0;

	while(i < 1000)
	{
		if(i % 3 == 0 || i % 5 == 0)
		{
			multiples++;
			answer += i;

			#if DISPLAY_MULTIPLES
			printf("%u\n", i);
			#endif
		}

		i++;
	}

	printf("DONE!\nThe answer, containing %u multiples, is %u.\n", multiples, answer);
}